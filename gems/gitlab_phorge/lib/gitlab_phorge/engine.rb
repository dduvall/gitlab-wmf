module GitlabPhorge
  class Engine < ::Rails::Engine
    config.after_initialize do
      # Unfortunately the set of all available integrations is defined as a
      # frozen array constant in the base class. Until there is a way to
      # register integrations that isn't coupled with the base class, we have
      # to do a little monkey patching.
      #
      # Note that Rails initializers can be thread unsafe, so we should be ok
      # on that front.
      ::Integration.const_set(
        :INTEGRATION_NAMES,
        (::Integration.module_exec { remove_const(:INTEGRATION_NAMES) } + ['phorge']).freeze
      )
    end
  end
end
