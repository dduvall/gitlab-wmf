# GitLab Phorge
Integrates Phorge with GitLab issue tracking.

## Usage
TODO

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'gitlab_phorge'
```

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
