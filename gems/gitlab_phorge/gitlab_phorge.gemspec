require_relative "lib/gitlab_phorge/version"

Gem::Specification.new do |spec|
  spec.name        = "gitlab_phorge"
  spec.version     = GitlabPhorge::VERSION
  spec.authors     = ["Dan Duvall"]
  spec.email       = ["Dan Duvall <dduvall@wikimedia.org>"]
  spec.homepage    = "https://gitlab.wikimedia.org/repos/releng/gitlab-phorge"
  spec.summary     = "Phorge integration for GitLab"
  spec.description = "Integrates GitLab with Phorge"
  spec.license     = "MIT"

  spec.metadata["homepage_uri"] = spec.homepage

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "README.md"]

  spec.add_dependency "rails", "~> 6.1.7", ">= 6.1.7.2"
end
