# gitlab-wmf

GitLab + WMF specific engines/railties:

 - `gitlab_phorge`: provides integration with Phorge. As of now, simply the
   rendering of task numbers as links to http://phabricator.wikimedia.org.

## Introduction

We need to provide additional functionality to our stakeholders that is
missing from GitLab's CE offering.

GitLab has no formal plugin APIs, and upstream seems [against the idea of
providing them][plugins]. GitLab's main service is built on Rails, however,
and Rails applications may load any number of [Rails Engines][engines] that
can acts as a subsystem of the main application, defining their own
initializers, routes, models, controllers, views, migrations, tasks, etc.

This project shows how we might inject our own `Gemfile` into GitLab's Rails
initialization process to load our own engines (or any gem really) that can
provide a path to extending GitLab in a standardized/module way.

## How it works

See the included [Docker Compose configuration](./docker-compose.yaml) and
corresponding [Dockerfile](./Dockerfile) for details. In conjunction, they:

 - Installs gems from our local [gems directory](./gems/) into the
   environment.
 - Install a `Gemfile.wmf` into the `gitlab-rails` service's working directory
   that:
   1. Loads upstream's `Gemfile`
   2. Defines an entry for each of our gems to be loaded as part of the
   default bundler group (Rails loads the default group in every environment).
 - Copies upstream's `Gemfile.lock` to `Gemfile.wmf.lock`
 - Executes `bundle lock` against `Gemfile.wmf` to update `Gemfile.wmf.lock`
   with entries for our gems. This is necessary because gems have been
   [frozen][frozen] in upstream's bundler config.
 - Adds the following configuration to `/etc/gitlab/gitlab.rb` to ensure our
   `Gemfile.wmf` file is used when Rails loads gems.

```ruby
gitlab_rails['env'] = {
  "BUNDLE_GEMFILE" => "/opt/gitlab/embedded/service/gitlab-rails/Gemfile.wmf",
}
```

## How to test this

```sh
docker compose up --build
```

Wait for GitLab to be configured and start for the first time (this takes a
while). Then go to http://gitlab.local.wmftest.net:8084 and log in
(username/password `root`/`password`).

Create a new project and go to Settings -> Integrations. Enable "Phorge"
integration and fill out the form with the right website and issue URLs.

Clone the project repo, create a local commit that references a Phab/Phorge
task, and push it to create a merge request.

The task ID in the MR should rendered as a link to Phabricator/Phorge.

[plugins]: https://about.gitlab.com/blog/2019/09/27/plugin-instability/
[engines]: https://guides.rubyonrails.org/engines.html
[frozen]: https://bundler.io/v1.12/man/bundle-config.1.html#LIST-OF-AVAILABLE-KEYS
